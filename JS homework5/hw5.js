'use strict';

function CreateNewUser(firstName, lastName) {
  this.firstName = prompt('Enter your first name');
  this.lastName = prompt('Enter your last name');
  this.birthday = prompt('Enter your birthday in format "dd.mm.yyyy"');
}

const newUser = new CreateNewUser(this.firstName, this.lastName);
newUser.getLogin = function () {
  return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
};
newUser.getPassword = function () {
  return (
    this.firstName[0].toUpperCase() +
    this.lastName.toLowerCase() +
    this.birthday[6] +
    this.birthday[7] +
    this.birthday[8] +
    this.birthday[9]
  );
};
newUser.getAge = function () {
  const myBirthday = this.birthday; //'15.06.1992';
  const mySplit = myBirthday.split('.');
  mySplit.reverse();
  const newDate = mySplit.join('-');
  Date.parse(newDate);
  //console.log(Date.parse(newDate));
  const age = Date.now() - Date.parse(newDate);
  //console.log(age);
  return Math.floor(age / 1000 / (60 * 60 * 24) / 365.25);
};

console.log(newUser);
console.log(newUser.getAge() + ' years');
console.log(newUser.getLogin());
console.log(newUser.getPassword());

/*
1. Опишите своими словами, что такое экранирование, 
и зачем оно нужно в языках программирования.
Экранирование - это способ заключения в кавычки одиночного символа. 
Обратный слэш "\" - символ экранирования. Используется для того, чтобы
убедться что символы являются текстом, а не частью кода.
*/
