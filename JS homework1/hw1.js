"use strict"

let userName = prompt("Enter your name");
let userAge = prompt("Enter your age");

if (userAge < 18) {
    alert(`You are not allowed to visit this website`);
} else if (userAge > 22) {
        alert(`Welcome, ${userName}`);
} else if (userAge >= 18 && userAge <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert(`Welcome, ${userName}`)
    } else {
        alert("You are not allowed to visit this website");
    }
}

/*
1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

    Видимость переменной let ограничивается блоко (все, что в {}), var же видна везде в функции.
    Переменная let не существует до ее объявления (выдаст ошибку), var - существует до и равно undefined.
    Const - константа, которую нельзя переназначить.

2. Почему объявлять переменную через var считается плохим тоном?

    Согласно стандарту ES6 необходимо использовать для объявления переменных let и const, что дает больше
возможностей по контролю области видимости переменных в коде. Использование var не имеет смысла, лишь для
поддержки очень старых браузеров.
*/
