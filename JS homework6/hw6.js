'use strict';

const items = ['hello', 'world', 23, '23', null];
const filterBy = (arr, type) => arr.filter(item => typeof item !== type);

console.log(filterBy(items, 'string'));

/*
1. Опишите своими словами как работает цикл forEach.
Метод forEach перебирает элементы массива, ничего не возвращая. Принцип работы подобен циклу for,
при этом нельзя использовать оператор break. Для каждого элемента массива вызывается callback-функция,
где указываются item, его порядковый номер и само название массива.
*/
